class Node:
    def __init__(self, cargo=None, next=None):
        self.cargo = cargo
        self.next = next

    def __str__(self):
        return str(self.cargo)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, value):
        if isinstance(value, Node):
            return self.cargo == value.cargo
        return self.cargo == value

    def __hash__(self):
        return hash(cargo)

    def print_forward(self):
        node = self
        print('[', end='')
        while node:
            print(node, end='')
            node = node.next
            if node:
                print(', ', end='')
        print(']')

    def print_backward(self):
        if self.next is not None:
            self.next.print_backward()
        print(self.cargo, end=' ', flush=True)

class LinkedList:
    def __init__(self, sequel=None):
        self.__length = 0
        self.head = None

        if sequel is not None:
            # use reversed because we append to head
            for x in reversed(sequel):
                self.add(x)

    def __str__(self):
        node = self.head
        string = "["
        while node:
            string += str(node.cargo) + ", "
            node = node.next
        return string.strip(", ") + "]"

    def __len__(self):
        return self.__length

    def add(self, cargo):
        """Adds a new item to the beginning of the list, returns the created
        Node.
        """
        assert isinstance(self.__length, int)

        node = Node(cargo=cargo, next=self.head)
        self.head = node
        self.__length += 1
        return node

    def remove(self, cargo):
        """Remove item from LinkedList, returns the removed Node or Node case
        item is not found.
        """
        assert isinstance(self.__length, int)

        node = self.head
        previous = None
        while node:
            if node.cargo == cargo:
                if node == self.head:
                    self.head = node.next
                elif node.next is None:
                    previous.next = None
                else:
                    previous.next = node.next
                self.__length -= 1
                break
            previous = node
            node = node.next
        return node

    def search(self, cargo):
        """Search for an item in the list, returns tuple with Node and index.
        """
        if self.head is None:
            return None
        node = self.head
        idx = 0
        while node:
            if node.cargo == cargo:
                break
            node = node.next
            idx += 1
        return (node, idx)

    def __iter__(self):
        self.current_node = self.head
        return self

    def __next__(self):
        if self.current_node is not None:
            try:
                return self.current_node
            finally:
                self.current_node = self.current_node.next
        else:
            raise StopIteration

    def print_backward(self):
        self.head.print_backward()

    def print_forward(self):
        self.head.print_forward()