from data_structs import linked_list

class HashTable:
    def __init__(self, size=11):
        self.length = 0
        self.size = size
        self.table = [None] * size

    def hash_key(self, cargo):
        try:
            key = cargo.key
        except:
            key = cargo
        return key % self.size

    def add(self, cargo):
        hash_code = self.hash_key(cargo)
        if self.table[hash_code] is None:
            self.table[hash_code] = linked_list.LinkedList()
        self.table[hash_code].add(cargo)
        self.length += 1

    def remove(self, cargo):
        hash_code = self.hash_key(cargo)
        if self.table[hash_code] is not None:
            try:
                return self.table[hash_code].remove(cargo)
            finally:
                self.length -= 1
        return None

    def retrieve(self, cargo):
        hash_code = self.hash_key(cargo)
        if self.table[hash_code]:
            node, idx = self.table[hash_code].search(cargo)
        return node.cargo or None

    def __len__(self):
        return self.length
