from uni.university import *

MAIN_MENU_MSG = F"""
Bem vindo!

    1 - Inserir Aluno
    2 - Buscar Aluno
    3 - Remover Aluno
    4 - Exibir Tabela

    0 - Sair"""


def get_option(begin, end, out=0):
    while True:
        try:
            op = int(input("> "))
        except KeyboardInterrupt:
            print()
            exit(1)
        except:
            pass
        else:
            if not (op < begin or op > end):
                break
        print("Opção inválida.")
    return op


def main_menu(table):
    while True:
        try:
            print(MAIN_MENU_MSG)
            op = get_option(0, 4)
            if op == 1:
                print(insert_student(table))
            elif op == 2:
                result, found = search_student(table)
                if found:
                    print(result)
                input("\nPressione qualquer tecla pra voltar...")
            elif op == 3:
                remove_student(table)
            elif op == 4:
                list_all(table)
            elif op == 0:
                print("Tchau!")
                return
        except KeyboardInterrupt:
            exit(0)

def int_input(name, ptbr='a'):
    while True:
        try:
            registry = int(input(F"{name}: "))
        except KeyboardInterrupt:
            exit(1)
        except:
            print(F"{name} inválid{ptbr}, tente novamente.")
        else:
            return registry


def get_unique_registry(table):
    while True:
        registry = int_input("Matrícula")
        if table.search(registry) is None:
            return registry
        print("Matrícula já existe, tente novamente")


def insert_student(table):
    print("Insira os dados do Aluno")
    registry = get_unique_registry(table)
    name = input("Nome: ")
    while True:
        try:
            age = int(input("Idade: "))
        except KeyboardInterrupt:
            exit(1)
        except:
            print("Idade inválida, tente novamente.")
        else:
            break
    student = Student(registry, name, age)
    return table.add(student)


def search_student(table, action="Buscar"):
    print(F"\n{action}\nInforme a matrícula do aluno")
    registry = int_input("Matrícula")
    student = table.search(registry)
    if student is None:
        print(F"\nNenhum aluno encontrado com matricula: {registry}")
        return (None, False)
    print("\nEncontrado\n")
    return (student, True,)


def remove_student(table):
    if len(table) <= 0:
        print("Nenhum aluno cadastrado. Impossível remover.")
        return

    student, found = search_student(table, action='Remover')
    if found:
        print(F"Removendo {repr(student)}")
        if table.remove(student.registry):
            print("Aluno removido com sucesso.")
        else:
            print(F"Não foi possível remover.")

    question = "\nDeseja continuar removendo? (sim|s ou n|não): "
    if len(table) > 0 and input(question) in ('s','sim','y','yes'):
        remove_student(table)


def list_all(table):
    print("Mostrar tabela\n")
    print(table)
    input("\nPressione qualquer tecla pra voltar...")


if __name__ == '__main__':
    table = StudentRegistry()
    table.add(Student(13, 'chaves', 21))
    table.add(Student(15, 'kiko', 21))
    table.add(Student(12, 'madruga', 50))
    table.add(Student(24, 'girafalles', 45))
    table.add(Student(35, 'nhonho', 80))
    table.add(Student(23, 'chiquinha', 21))
    main_menu(table)

