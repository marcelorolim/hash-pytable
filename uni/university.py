from data_structs.hash_table import *


class Student:
    def __init__(self, registry, name, age):
        self.registry = registry
        self.name = name
        self.age = age

    @property
    def key(self):
        return self.registry

    def __str__(self):
        return F"matricula:{self.registry} \nnome:{self.name} \nidade:{self.age}"

    def __repr__(self):
        return F"matr:{self.registry}-{self.name}-{self.age}anos"

    def __hash__(self):
        assert isinstance(self.registry, int)
        return self.registry % 11

    def __eq__(self, value):
        if isinstance(value, Student):
            return self.registry == value.registry and \
                self.name == value.name and \
                self.age == value.age
        elif isinstance(value, int):
            return self.registry == value
        return False


class StudentRegistry(HashTable):

    def search(self, registry):
        """Retrieve by registry
        """
        s1 = Student(registry,"",2)
        hash_code = self.hash_key(registry)
        if self.table[hash_code]:
            for node in self.table[hash_code]:
                if node.cargo.registry == registry:
                    return node.cargo
        return None

    def add(self, student):
        """Override 'add' so we only insert available registries
        """
        if self.search(student.registry):
            return None
        return super().add(student)

    def remove(self, cargo):
        removed = super().remove(cargo)
        try:
            return removed.cargo
        except AttributeError:
            return removed

    def __repr__(self):
        # sep = '--------'
        sep = '========'
        string_table = ''
        for i, row in zip(range(len(self.table)), self.table):
            string_table += F'{sep}\n{i}\t'
            string_row = ''
            if row is None:
                string_row = '-'
            else:
                for node in row:
                    string_row += repr(node.cargo) + ' -> '
            string_table += string_row + '\n'
        return string_table + sep
