from unittest import TestCase
from data_structs.hash_table import *
from data_structs.linked_list import *

from uni.university import *


class TestLinkedList(TestCase):

    def setUp(self):
        # called before EACH test
        self.base = [1, 2, 3]
        self.linked_list = LinkedList(self.base)

    def test_head(self):
        self.assertEqual(1, self.linked_list.head)

    def test_len_function(self):
        self.assertEqual(len(self.base), len(self.linked_list))

    def test_add(self):
        self.linked_list.add(4)
        self.linked_list.add(5)
        self.assertEqual(5, self.linked_list.head)

    def test_remove_head(self):
        self.linked_list.remove(1)
        self.assertEqual(self.linked_list.head, 2)
        self.assertEqual(len(self.linked_list), 2)

    def test_remhash_key(self):
        self.linked_list.remove(2)
        self.assertEqual(self.linked_list.head, 1)
        self.assertEqual(len(self.linked_list), 2)

    def test_remove_tail(self):
        self.linked_list.remove(3)
        self.assertEqual(self.linked_list.head, 1)
        self.assertEqual(len(self.linked_list), 2)

    def test_remove_non_existing(self):
        not_found = self.linked_list.remove(4)
        self.assertIsNone(not_found)
        self.assertEqual(len(self.linked_list), 3)

    def test_search(self):
        item = 3
        result = self.linked_list.search(item)

        self.assertEqual(result[0].cargo, item)
        self.assertEqual(result[1], 2)

    def test_string_representation(self):
        self.assertEqual(str(self.linked_list), "[1, 2, 3]")

    def test_multiple_operations(self):
        self.linked_list.add(4)
        self.linked_list.add(5)
        self.assertEqual(str(self.linked_list), "[5, 4, 1, 2, 3]")
        self.assertEqual(len(self.linked_list), 5)

        self.linked_list.remove(1)
        self.linked_list.remove(4)
        self.assertEqual(str(self.linked_list), "[5, 2, 3]")
        self.assertEqual(len(self.linked_list), 3)

        self.linked_list.add(6)
        self.assertEqual(str(self.linked_list), "[6, 5, 2, 3]")
        self.assertEqual(len(self.linked_list), 4)

    def test_table_iterator(self):
        l = []
        for i in self.linked_list:
            l.append(i.cargo)
        self.assertEqual(str(self.linked_list), str(l))


class TestStudentRegistry(TestCase):

    def setUp(self):
        self.table = StudentRegistry()
        self.student1 = Student(1, 'chaves', 21)
        self.student2 = Student(12, 'chiquinha', 22)
        self.student3 = Student(3, 'kiko', 23)

        self.table.add(self.student1)
        self.table.add(self.student2)
        self.table.add(self.student3)

    def test_retrieve(self):
        s1 = Student(1, 'chaves', 21)
        get_student = self.table.retrieve(s1)
        self.assertEqual(get_student, s1)

    def test_search(self):
        get_student = self.table.search(self.student3.registry)
        self.assertEqual(get_student, self.student3)

    def test_add_existing_registry(self):
        s = Student(12, 'seu madruga', 45)
        self.assertEqual(self.table.add(s), None)

    def test_remove_non_existing(self):
        s_removed = self.table.remove(20)
        self.assertEqual(s_removed, None)

    def test_remove_existing(self):
        s = Student(12, 'seu madruga', 45)
        s_removed = self.table.remove(12)
        self.assertEqual(s_removed.registry, s)
        self.assertEqual(s_removed.registry, 12)